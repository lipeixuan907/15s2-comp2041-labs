#!/usr/bin/perl -w

$course = $ARGV[0];
@courses = ();
$ugURL = "http://www.handbook.unsw.edu.au/undergraduate/courses/2016/$course.html";
$pgURL = "http://www.handbook.unsw.edu.au/postgraduate/courses/2015/$course.html";

open UG, "wget -q -O- $ugURL|" or die;
while ($line = <UG>) {
	chomp @line;
	&processing($line);
}

open PG, "wget -q -O- $pgURL|" or die;
while ($line = <PG>) {
	chomp @line;
    &processing($line);
}

@courses = sort @courses;
foreach $preCourse (@courses) {
	print "$preCourse\n";
}

sub processing {
	$line = $_[0];
	if ($line =~ /Prerequisite/) {
		$line =~ s!</p>.*</p>!!g;
		@array = ($line =~ /[A-Z]{4}[0-9]{4}/g);
		push @courses, @array;
	}
}