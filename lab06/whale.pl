#!/usr/bin/perl -w

$targetName = $ARGV[0];
$numberOfPods = 0;
$numOfIndividuals = 0;

while ($line = <STDIN>) {
	chomp $line;
	$line =~ /^(\d+) (.*)$/;
	$number = $1;
	$whaleName = $2;
	if ($whaleName eq $targetName) {
		$numberOfPods++;
		$numOfIndividuals += $number;
	}
}

print "$targetName observations: $numberOfPods pods, $numOfIndividuals individuals\n";