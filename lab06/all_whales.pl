#!/usr/bin/perl -w

%numOfPods = ();
%numOfIndividuals = ();
while ($line = <STDIN>) {
	chomp $line;
	# transfer capital letters into lower case
	$line =~ tr/[A-Z]/[a-z]/;
	# remove last 's'
	$line =~ s/s$//g;
	# remove duplicated spaces
	$line =~ s/\s+/ /g;
	# remove spaces at tail
	$line =~ s/\s$//g;
	
	# get infomation
	$line =~ /^(\d+)\s+(.*)$/;
	$number = $1;
	$whaleName = $2;

	# save information into hash
	if (exists $numOfPods{$whaleName}) {
		$numOfPods{$whaleName}++;
		$numOfIndividuals{$whaleName} += $number;
	} else {
		$numOfPods{$whaleName} = 1;
		$numOfIndividuals{$whaleName} = $number;
	}
}
# get whale names and sort
@whales = keys %numOfPods;
@whales = sort @whales;
# print result
foreach $whale (@whales) {
	print "$whale observations:  $numOfPods{$whale} pods, $numOfIndividuals{$whale} individuals\n";
}