#!/bin/sh

for file in "$@"
do
	display $file
	echo -e 'Address to e-mail this image to? \c'
	read email
	echo -e 'Message to accompany image? \c'
	read message
	echo "$file sent to $email"
	echo $message | mutt -s 'COMP9041 Lab04' -a $file -- $email
done