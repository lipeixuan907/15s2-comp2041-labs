#!/bin/sh

for file in "$@"
do
	time="`ls -l "$file" | cut -d' ' -f6-8`"
	tempFile="temp_$file"
	mv "$file" "$tempFile"
	convert -gravity south -pointsize 36 -draw "text 0,10 '$time'" "$tempFile" "$file"
	touch -r "$tempFile" "$file"
	rm "$tempFile"
done