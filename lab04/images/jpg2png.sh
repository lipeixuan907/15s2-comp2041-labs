#!/bin/sh

for file in *.jpg
do
	prefix=`basename "$file" .jpg`
	if ls | egrep "^$prefix.png$" >/dev/null
	then
		echo "$prefix.png already exists"
		# continue
		exit 1
	fi
	convert "$prefix.jpg" "$prefix.png"
	rm "$prefix.jpg"
done