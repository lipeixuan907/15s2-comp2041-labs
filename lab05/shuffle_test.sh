#!/bin/sh

i=0;while test $i -lt 5; do echo $i; i=$((i + 1)); done > temp1
cat temp1 | ./shuffle.pl > temp2

# test whether output is same as original
if diff temp1 temp2 > /dev/null
	then
	echo "Outputs are same, please try again"
fi

# test number of line
n1=`wc -l temp1`
n2=`wc -l temp2`
if test n1 -ne n2
	then
	echo "Output has different number of line, please check your code"
	exit
fi

# test all lines
i=0;
while test $i -lt 5
do
	if ! cat temp2 | egrep "^$i$" > /dev/null
		then
		echo 'test failed, please check your code'
		exit
	fi
	i=`expr $i + 1`
done

echo "test passed, good job"