#!/usr/bin/perl -w

@arg = @ARGV;
# Number of line
$n = 10;
if (($#ARGV+1 > 0) && ($ARGV[0] =~ /^-[0-9]+$/)) {
	$n = shift @arg;
	$n =~ s/-//gi;
}

# STDIN
if ($#arg+1 == 0) {
	@input = ();
	while (<STDIN>){ 
		chomp($l = $_);
		push @input, $l;}
	if ($#input+1 <= $n) {
		foreach $line (@input) {print $line, "\n"}
	} else {
		for ($i = $#input + 1 - $n; $i < $#input + 1; $i++) {
			print $input[$i], "\n";
		}
	}
}
# Open from files
else {
	foreach $f (@arg){
	open(F,"<$f") or die "$0: can't open $f\n";
	if ($#arg > 0) {
		print "==> $f <==\n";
	}
	@input = ();
	while (<F>){ 
		chomp($l = $_);
		push @input, $l;}
	if ($#input+1 <= $n) {
		foreach $line (@input) {print $line, "\n"}
	} else {
		for ($i = $#input + 1 - $n; $i < $#input + 1; $i++) {
			print $input[$i], "\n";
		}
	}

	close(F);
	}
}