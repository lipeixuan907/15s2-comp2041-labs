#!/usr/bin/perl -w
if ($#ARGV+1 != 2) {
	die "Usage: ./echon.pl <number of lines> <string>";
}
$n = $ARGV[0];
$text = $ARGV[1];
while ($n > 0) {
	print $text,"\n";
	$n--;
}