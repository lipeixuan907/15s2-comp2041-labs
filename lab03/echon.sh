#!/bin/sh

# test number of arguments
if test $# != 2
then
	echo 'Usage: ./echon.sh <number of lines> <string>'
	exit 1
elif [[ $1 =~ [^0-9] ]]
then
	echo './echon.sh: argument 1 must be a non-negative integer'
	exit 1
fi

n=$1
text=$2
while test $n -gt 0
do
	echo $text
	n=`expr $n - 1`
done