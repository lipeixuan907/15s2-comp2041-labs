#!/bin/sh

wget -q -O- "http://www.handbook.unsw.edu.au/vbook2015/brCoursesByAtoZ.jsp?StudyLevel=Undergraduate&descr=All" | egrep 'http://www.handbook.unsw.edu.au/undergraduate/courses/2015/' | cut -d'/' -f7-8 | sed -e "s/\/A>//g" | sed -e "s/\.html\">/ /g" | sed -e "s/<//g" | egrep $1[0-9]{4} >>temp
wget -q -O- "http://www.handbook.unsw.edu.au/vbook2015/brCoursesByAtoZ.jsp?StudyLevel=Postgraduate&descr=All" | egrep 'http://www.handbook.unsw.edu.au/postgraduate/courses/2015/' | cut -d'/' -f7-8 | sed -e "s/\/A>//g" | sed -e "s/\.html\">/ /g" | sed -e "s/<//g" | egrep $1[0-9]{4} >>temp
egrep $1 temp | sort | sed -e "s/ $//g" | uniq
rm temp