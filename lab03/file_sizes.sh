#!/bin/sh

for file in *
do
	n=`wc -l $file | cut -d' ' -f1`
	if test $n -lt 10
		then echo "$file SMALL" >>temp
	elif test $n -lt 100
		then echo "$file MEDIUM" >>temp
	else echo "$file LARGE" >>temp
	fi
done

echo Small files: `egrep ' SMALL$' temp | cut -d' ' -f1 | sort`
echo Medium-sized files: `egrep ' MEDIUM' temp | cut -d' ' -f1 | sort`
echo Large files: `egrep ' LARGE' temp | cut -d' ' -f1 | sort`

rm temp