#!/usr/bin/perl -w

$numOfWords = 0;
while ($line = <STDIN>) {
	chomp $line;
	@words = ($line =~ /[a-zA-Z]+/g);
	$numOfWords += $#words + 1;
}
print "$numOfWords words\n";