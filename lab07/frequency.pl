#!/usr/bin/perl -w

$targetWord = $ARGV[0] or die;
%numOfTotalWords = ();
%numOfTargetWord = ();

foreach $file (glob "poets/*.txt") {
	$file =~ /poets\/(.+)\.txt/g;
	$fileName = $1;
	$numOfTargetWord{$fileName} = 0;
	open F, "$file" or die "Cannot open file: $!";
	while ($line = <F>) {
		chomp $line;
		$line = lc $line;
		@words = ($line =~ /[a-zA-Z]+/g);
		# count total words
		if (exists $numOfTotalWords{$fileName}) {
			$numOfTotalWords{$fileName} += $#words + 1;
		}else {$numOfTotalWords{$fileName} = $#words + 1}
		# count target word
		foreach $word (@words) {
			if ($word =~ /^$targetWord$/i){
				$numOfTargetWord{$fileName}++;
			}
		}
	}
}
@fileNames = keys %numOfTotalWords;
@fileNames = sort @fileNames;
foreach $poet (@fileNames) {
	$poetName = $poet;
	$poetName =~ s/[^a-z]/ /gi;
	printf "%4d/%6d = %.9f %s\n", 
		$numOfTargetWord{$poet}, $numOfTotalWords{$poet}, $numOfTargetWord{$poet} / $numOfTotalWords{$poet}, $poetName;	
}