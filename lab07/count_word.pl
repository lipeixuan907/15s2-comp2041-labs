#!/usr/bin/perl -w

$targetWord = $ARGV[0];
$targetWord = lc $targetWord;
$numOfWord = 0;
while ($line = <STDIN>) {
	chomp $line;
	$line = lc $line;
	@words = ($line =~ /[a-z]+/g);
	foreach $word (@words) {
		if ($word =~ /^$targetWord$/){
			$numOfWord++;
		}
	}
}
print "$targetWord occurred $numOfWord times\n";