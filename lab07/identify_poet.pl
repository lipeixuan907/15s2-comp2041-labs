#!/usr/bin/perl -w

if ($ARGV[0] =~ /^-d$/) {
	$debug = 1;
	shift @ARGV;
}

# Preprocessing the "database".
# count number of exist times for every words in each poet in database and save them into hash
foreach my $poet (glob "poets/*.txt"){
	open POET, "$poet" or die "Cannot open file: $!";
	$poet =~ /poets\/(.+)\.txt/g;
	$poetName = $1;
	while (my $line = <POET>){
		chomp $line;
		$line = lc $line;
		my @words = ($line =~ /[a-z]+/g);
		foreach my $word (@words){
			$numOfWord{$poetName}{$word}++;
		}
	}
}

# Count the total number of words for each poet in database.
foreach my $poet (sort keys %numOfWord){
	foreach my $word (keys %{$numOfWord{$poet}}){
		$totalNumOfWords{$poet} += $numOfWord{$poet}{$word};
	}
}

# get results for every text file
foreach my $file (@ARGV){
	open FILE, "$file" or die "Cannot open file: $!";
	# compute and sum log probabilities for each file
	while (my $line = <FILE>) {
		chomp $line;
		$line = lc $line;
		my @words = ($line =~ /[a-z]+/g);
		foreach my $word (@words){
			foreach my $poet (keys %numOfWord){
				if (exists $numOfWord{$poet}{$word}) {
					$logProb{$poet} += log(($numOfWord{$poet}{$word}+1) / $totalNumOfWords{$poet});
				} else {
					$logProb{$poet} += log(1 / $totalNumOfWords{$poet})
				}
			}
		}
	}
	my @results = sort {$logProb{$b} <=> $logProb{$a}} keys %logProb;
	my $name = $results[0];
	$name =~ s/[^a-z]/ /gi;
	
	# debugging
	if (defined $debug) {
		foreach my $result (@results){
			my $name = $result;
			$name =~ s/[^a-z]/ /gi;
			printf "%s: log_probability of %8.1f for %s\n", $file, $logProb{$result}, $name;
		}
	}
	# print result
	printf "%s most resembles the work of %s (log-probability=%8.1f)\n", $file, $name, $logProb{$results[0]};
	# clear hash of %logProb
	undef %logProb;
}