#!/usr/bin/perl -w
@courses = @ARGV;
# check for options
if ($courses[0] =~ /^-[dt]$/){
	$option = shift @courses;
	# $option =~ tr/-//;
}

%mark = ();
@periods = ();

foreach my $course_code (@courses) {
	my $url = "http://www.timetable.unsw.edu.au/current/$course_code.html";
	open WEB, "wget -q -O- $url|" or die "Cannot open url";
	while (my $line = <WEB>) {
		chomp $line;
		# target line is followed by a line which catians word "Lecture"
		if ($line =~ /Lecture/) {
			$counter = 0;
			$line =~ /#([A-Z][12])-/g;
			$period = $1;
		}elsif (defined $counter) {	#find the target line
			if ($counter == 5 && $line =~ /Weeks/){
				# get information
				$line =~ s/<.*>(.*)<.*>/$1/g;
				# remove useless spaces
				$line =~ s/\s*([a-z].*)/$1/i;
				# connect result
				my $text = $course_code . ": " . $period . " $line\n";
				push @periods, $period;
				if (exists $mark{$text}) {
					$mark{$text}++;
				} else {
					# if has option
					if(defined $option){
						# -d option
						&option_d($text) if $option eq "-d";
						&option_t($text) if $option eq "-t";
					}
					# no options
					else{
						print $text;
					}
					$mark{$text} = 1;
				}
				undef $counter;
			}else{
				$counter++;
			}
		}
	}
}

&printTable;
## Functions
# processing -d option
sub option_d($) {
	$_[0] =~ /([A-Z]{4}[0-9]{4})/;
	my $course_code = $1;
	my @times = ($_[0] =~ /[a-z]{3} [0-9]{2}:[0-9]{2} - [0-9]{2}:[0-9]{2}/gi);
	
	foreach my $time (@times){
		$time =~ /([a-z]{3}) ([0-9]{2}):[0-9]{2} - ([0-9]{2}:[0-9]{2})/i;
		my $day = $1;
		my $start = $2;
		if ($start < 10){$start =~ s/0//;}
		my $end = $3;
		$end =~ tr/:/./;

		while ($start < $end) {
			print $period . " " . $course_code . " " . $day . " " . $start . "\n";
			$start++;
		}
	}
}

# processing -t option
sub option_t($){
	$_[0] =~ /([A-Z]{4}[0-9]{4})/;
	my $course_code = $1;
	my @times = ($_[0] =~ /[a-z]{3} [0-9]{2}:[0-9]{2} - [0-9]{2}:[0-9]{2}/gi);
	
	foreach my $time (@times){
		$time =~ /([a-z]{3}) ([0-9]{2}):[0-9]{2} - ([0-9]{2}:[0-9]{2})/i;
		my $day = $1;
		my $start = $2;
		if ($start < 10){$start =~ s/0//;}
		my $end = $3;
		$end =~ tr/:/./;

		while ($start < $end) {
			# count number of courses at the same time
			$timetable{$period}{$start}{$day}++;
			$start++;
		}
	}
}

# print the time table
sub printTable {
	foreach my $period (sort keys %timetable){
		# print title
		print "$period       Mon   Tue   Wed   Thu   Fri\n";
		for (my $i = 9; $i <= 20; $i++){
			if ($i == 9){print "0"}
			print $i . ":00";
			# if has course at this time
			if (exists $timetable{$period}{$i}) {
				foreach my $day (Mon, Tue, Wed, Thu, Fri){
					if (exists $timetable{$period}{$i}{$day}) {
						printf "%6d", $timetable{$period}{$i}{$day};

					} else {
						print "      ";
					}
				}
				print "\n";
			} else {print "\n"}

		}
	}
}