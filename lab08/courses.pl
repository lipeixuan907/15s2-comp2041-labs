#!/usr/bin/perl -w
$prefix = $ARGV[0];
$prefixKENS = $prefix . "KENS";
$url = "http://www.timetable.unsw.edu.au/current/$prefixKENS.html";
@courses = ();
%flag = ();

open WEB, "wget -q -O- $url|" or die "Cannot open url";
while (my $line = <WEB>) {
	chomp $line;
	if (my @classes = ($line =~ /$prefix[0-9]{4}/g)) {
		foreach my $class (@classes) {
			if (exists $flag{$class}) {
				$flag{$class}++;
			} else {
				push @courses, $class;
				$flag{$class} = 1;
			}
		}
	}
}
# print results
foreach my $course (@courses) {
	print "$course\n";
}