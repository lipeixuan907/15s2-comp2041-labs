== Question 1 == 
a) What word is on line 2000 

Less keystrokes:	2000G
Word:	Algonquian

b) the first word in the dictionary file ending with 's

Less keystrokes:	/z$
Word:	Abkhaz

c) the last word in the dictionary that contains the substring ooz ?

Less keystrokes:	G	?ooz
Word:	zoozoos


== Question 2 == 

a) How many total words does the file contain?  

Command:	wc -w words.txt | cut -d' ' -f1
Answer:	390582

b) How many total words, excluding those ending in "'s", are there in the dictionary?

Pipeline:	egrep -v \'s$ words.txt | wc -l
Answer:	299846

c) How could you use cat -n and grep to find out which word is on line 100000? 

Pipeline:	cat -n words.txt | egrep 100000 | cut -f2
Answer:	adviser

d) How could you use cat -n and grep to print the 700th line, and no other lines?

Pipeline:	cat -n words.txt | egrep '[^0-9]700[^0-9]' | cut -f2
Answer:	 700  Adirondack


e) How do you use head and tail to find out what word is on line 200000 

Pipeline:	head -200000 words.txt | tail -1
Answer:	geodynamics's

== Question 3 == 
a) which words contain the characters "lmn" consecutively?

Pipeline:	egrep lmn words.txt
Answer:	
Selmner
Selmner's
almner
almners
calmness
calmness's
calmnesses


b) how many words contain "zz", but do not end in apostrophe-s ('s)?

Pipeline:	egrep zz words.txt | egrep -v "'s$" | wc -l
Answer:	628

c) how many words contain four consecutive vowels?

Pipeline:	egrep [aeiou]{4} words.txt | wc -l
Answer:	205

d) which English words contain all 5 english vowels "aeiou" in that order? 

Pipeline:	egrep 'a.*e.*i.*o.*u' words.txt
Answer:	abstemious, abstemiously, abstemiousness, abstemiousness's, abstemiousnesses, abstentious, adenocarcinomatous, adventitious, adventitiously, adventitiousness
		adventitiousness's, adventitiousnesses, aeruginous, amentiferous, androdioecious, andromonoecious, anemophilous, antenniferous, antireligious, arenicolous
		argentiferous, arsenious, arteriovenous, asclepiadaceous, autoecious, autoeciously, bacteriophagous, caesalpiniaceous, caesious, cavernicolous,
		chaetiferous, facetious, facetiously, facetiousness, facetiousness's, facetiousnesses, flagelliferous, garnetiferous, haemoglobinous, hamamelidaceous
		lateritious, paroecious, quadrigeminous, sacrilegious, sacrilegiously, sacrilegiousness, sacrilegiousness's, sacrilegiousnesses, sarraceniaceous, supercalifragilisticexpialidocious, 
		ultrareligious, ultraserious, valerianaceous

e) how many English words contain all 5 english vowels "aeiou" in that order? 

Pipeline:	egrep 'a.*e.*i.*o.*u' words.txt | wc -l
Answer:	53

f) Challenge which English words contain exactly 5 vowels and the vowels are "aeiou" in that order? 

Pipeline:	egrep 'a.*e.*i.*o.*u' words.txt | egrep -v '(a.*a)|(e.*e)|(i.*i)|(o.*o)|(u.*u)'
Answer:
abstemious
abstemiously
abstentious
arsenious
caesious
facetious
facetiously


g) How many 10+ letter lower case words which contain a 9 character lower-case word.

Pipeline:	egrep -w '^[a-z]{9}$' words.txt > words9.txt
		wc -l words9.txt	#36148
		fgrep -f words9.txt words.txt | egrep -v \' | egrep '.{10,}' | wc -l
Answer:	30213

== Question 4 == 

Pipeline:
Answer:

a) Write a grep command that will print all the lines in the file where the electorate begins with W.

Pipeline:	egrep 'for W' parliament.txt
Answer:
The Hon Tony Abbott, Member for Warringah  
Mr Scott Buchholz, Member for Wright  
The Hon Tony Burke, Member for Watson
Mr Nick Champion, Member for Wakefield  
Mr Laurie Ferguson, Member for Werriwa  
Mr Dan Tehan, Member for Wannon  
Mr Kelvin Thomson, Member for Wills  
The Hon Warren Truss, Member for Wide Bay
The Hon Malcolm Turnbull, Member for Wentworth

b) Write a grep command that will list all the lines in the file where the MP's first name is Andrew. 

Pipeline:	egrep 'Andrew ' parliament.txt
Answer:	
Mr Andrew Laming, Member for Bowman  
Dr Andrew Leigh, Member for Fraser  
The Hon Andrew Robb, Member for Goldstein  
Dr Andrew Southcott, Member for Boothby  
Mr Andrew Wilkie, Member for Denison

c) Write a grep command that will print all the lines in the file with three consecutive vowels.

Pipeline:	egrep [aeiou]{3} parliament.txt	
Answer:	Mrs Louise Markus, Member for Macquarie

d) Write a grep command that will print all the lines in the file where the MP's surname (last name) ends in the letter 'y'.

Pipeline:	egrep 'y,' parliament.txt
Answer:	
The Hon David Bradbury, Member for Lindsay  
Mr Michael Danby, Member for Melbourne Ports
The Hon Gary Gray, Member for Brand
The Hon Joe Hockey, Member for North Sydney  
Mr Craig Kelly, Member for Hughes  
The Hon Dr Mike Kelly, Member for Eden-Monaro
The Hon Sussan Ley, Member for Farrer  
The Hon John Murphy, Member for Reid
Mr Rowan Ramsey, Member for Grey  
Mr Wyatt Roy, Member for Longman  
The Hon Alexander Somlyay, Member for Fairfax

e) Write a grep command that will print all the lines in the file where the electorate ends in the letter 'y'.

Pipeline:	egrep 'y[^a-z]*$' parliament.txt	
Answer:
The Hon Bruce Billson, Member for Dunkley  
The Hon David Bradbury, Member for Lindsay  
The Hon Joe Hockey, Member for North Sydney  
Mr Ed Husic, Member for Chifley  
Dr Dennis Jensen, Member for Tangney  
Mr Stephen Jones, Member for Throsby  
The Hon Bob Katter, Member for Kennedy  
The Hon Tanya Plibersek, Member for Sydney  
Mr Rowan Ramsey, Member for Grey  
Mr Bernie Ripoll, Member for Oxley  
Ms Michelle Rowland, Member for Greenway
The Hon Tony Smith, Member for Casey  
Dr Andrew Southcott, Member for Boothby  
The Hon Dr Sharman Stone, Member for Murray  
The Hon Wayne Swan, Member for Lilley
The Hon Warren Truss, Member for Wide Bay

f) Write a grep command to print all the lines in the file where there is a word in the MP's name or the electorate name ending in ng.

Pipeline:	egrep '(ng[^a-z]+)|(ng$)' parliament.txt
Answer:	
Mr John Alexander, Member for Bennelong  
Mr Josh Frydenberg, Member for Kooyong  
Mr Michael Keenan, Member for Stirling  
The Hon Catherine King, Member for Ballarat  
Mr Andrew Laming, Member for Bowman  
Mr Don Randall, Member for Canning
The Hon Bill Shorten, Member for Maribyrnong

g) Write a grep command that will print all the lines in the file where the MP's surname (last name) both begins and ends with a vowel. 

Pipeline:	egrep ' [AEIOU][a-z]*[aeiou],' parliament.txt
Answer:	The Hon Anthony Albanese, Member for Grayndler

h) Write a grep command that will print all the lines in the file where the electorate name contains multiple words (separated by spaces or hyphens). 

Pipeline:	egrep -v 'for [A-Za-z]*[^ -]*[^a-z]*$' parliament.txt
Answer:	
The Hon Mark Butler, Member for Port Adelaide  
Mr Michael Danby, Member for Melbourne Ports
The Hon Peter Garrett, Member for Kingsford Smith
The Hon Joe Hockey, Member for North Sydney  
The Hon Dr Mike Kelly, Member for Eden-Monaro
Ms Laura Smyth, Member for La Trobe
The Hon Warren Truss, Member for Wide Bay
Mr Tony Windsor, Member for New England

== Question 5 == 

Write a shell pipline which prints the 2nd most common MP first name. 

Pipeline:	sed -e "s/.* \([A-Za-z]*\) [A-Za-z\']*,.*/\1/g" parliament.txt | sort | uniq -c | sort -k1 | tail -2 | head -1 | sed -e 's/.*[0-9] //'
Answer:	Andrew

== Question 6 ==

a) How many total classes are there?

Pipeline:	wc -l classes.txt | cut -d' ' -f1
Answer:	316

b) How many different courses have classes?

Pipeline:	cut -f1 classes.txt | sort | uniq -c | wc -l
Answer:	42


c) Write a pipeline which will print the course with the most classes?

Pipeline:	cut -f1 classes.txt | sort | uniq -c | sort -k 1 | tail -1 | cut -d' ' -f7
Answer:	ENGG1811

d) Give a command line that prints the most frequently-used tut room? 

Pipeline:	egrep 'TLB|TUT' classes.txt | cut -f5 | sort | uniq -c | sort -k 1 | tail -1 | cut -d' ' -f7-8
Answer:	Quad G040

e) Give a command line that prints the most popular time-of-day for tutes? 

Pipeline:	egrep 'TUT' classes.txt | cut -f4 | cut -c5-9 | sort | uniq -c | sort -k 1 | tail -1 | cut -d' ' -f7
Answer:	14-15

f) Which COMP courses run the most simultaneous classes of the same type? 

Pipeline:	egrep 'COMP' classes.txt | cut -f1,4 | sort | uniq -c | sort -k 1 | tail -2 | cut -d' ' -f8-9
Answer:	
COMP9414        Wed 16-18
COMP9814        Wed 16-18

g) Write a pipeline that prints list of the course names (only) of COMP courses that run simultaneous classes of the same type?

Pipeline:	egrep 'COMP' classes.txt | cut -f1,3,4 | sort | uniq -c | egrep -v '1 COMP' | sed -e 's/.*[0-9] \(COMP[0-9]*\).*/\1/g' | sort | uniq
Answer:	
	COMP0011
	COMP1911
	COMP1917
	COMP1927
	COMP2911
	COMP3141
	COMP3411
	COMP4511
	COMP9318
	COMP9321
	COMP9332
	COMP9333
	COMP9414
	COMP9814

== Question 7 (Challenge) ==

a) Match any line containing at least one A and at least one B.

Regexp:	A.*B|B.*A

b) Match any line any line containing only the characters A and B such that all pairs of adjacent As occur before any pairs of adjacent Bs

Regexp:	
^((BA+)*A*)*((BBA?)*(BA?))*$
# egrep -v '[AB]*BB[AB]*AA[AB]*' file1

c) Match any line containing  only the characters A and B such that all number of A's is divisible by 4.

Regexp:	(B*AB*AB*AB*AB*)+

d) Match any line containing n As followed by n Bs and no other characters..

Regexp/script:

